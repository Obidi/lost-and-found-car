import React, { Component } from 'react'
import {Form, Container, Divider, Image, Button, Select, Card} from 'semantic-ui-react'
import img1 from '../image/1.jpg'
import img2 from '../image/2.jpg'



export default class LostCarPage extends  Component{
    render(){
        return(
            <div className='lost'>
                <Container>
                    <h1 style={{paddingTop: '5em'}}>Seaching For a Car?</h1>
                    <Divider />
                    <Divider hidden/>

                    <Form style={{marginTop: '5em'}}>
                        <Form.Group >
                            <Form.Select color='blue' id='form-input-control-first-name' control={Select} label='What type of Car?' placeholder='Car Name' width={5}/>
                            <Form.Select id='form-input-control-first-name' control={Select} label='What Model of Car?' placeholder='Car Model' width={5}/>
                            <Form.Input type='date' label='Lost On:' placeholder='Date' width={3}/>
                            <Form.Input type='time' label='At Exactly:' placeholder='Time' width={3}/>
                        </Form.Group>

                        <Form.TextArea id='form-textarea-control-opinion'  label='Where did you loose it?' placeholder='Street, town and state' />
                        <Form.Field id='form-button-control-public' control={Button} color='teal' size='large' content='Search' label='Click to Search' />
                    </Form>

                    <Divider hidden/>
                    <Divider hidden/>
                    <h2 style={{textAlign: 'center'}}>List Of Found Cars</h2>
                    <Divider />

                    <Card.Group itemsPerRow={5}>
                        <Card>
                            <Card
                                image={img1}
                                header='2017 Prado Jeep'
                                meta='On 4/16/18 10:15am'
                                description='Found at Los Angeles, CA, USA'/>
                            <Card.Content extra>
                                <div className='ui two buttons'>
                                    <Button  color='teal'>Contact</Button>
                                </div>
                            </Card.Content>
                        </Card>
                        <Card>
                            <Card
                                image={img1}
                                header='2017 Prado Jeep'
                                meta='On 4/16/18 10:15am'
                                description='Found at Los Angeles, CA, USA'/>
                            <Card.Content extra>
                                <div className='ui two buttons'>
                                    <Button  color='teal'>Contact</Button>
                                </div>
                            </Card.Content>
                        </Card>
                        <Card>
                            <Card
                                image={img1}
                                header='2017 Prado Jeep'
                                meta='On 4/16/18 10:15am'
                                description='Found at Los Angeles, CA, USA'/>
                            <Card.Content extra>
                                <div className='ui two buttons'>
                                    <Button  color='teal'>Contact</Button>
                                </div>
                            </Card.Content>
                        </Card>
                        <Card>
                            <Card
                                image={img1}
                                header='2017 Prado Jeep'
                                meta='Friend'
                                description='Found at Los Angeles, CA, USA'/>
                            <Card.Content extra>
                                <div className='ui two buttons'>
                                    <Button  color='teal'>Contact</Button>
                                </div>
                            </Card.Content>
                        </Card>
                        <Card>
                            <Card
                                image={img1}
                                header='2017 Prado Jeep'
                                meta='On 4/16/18 10:15am'
                                description='Found at Los Angeles, CA, USA'

                            />
                            <Card.Content extra>
                                <div className='ui two buttons'>
                                    <Button  color='teal'>Contact</Button>
                                </div>
                            </Card.Content>
                        </Card>
                    </Card.Group>

                        <Card.Group itemsPerRow={5}>
                        <Card>
                            <Card
                                image={img2}
                                header='2017 Prado Jeep'
                                meta='On 4/16/18 10:15am'
                                description='Found at Los Angeles, CA, USA'/>
                            <Card.Content extra>
                                <div className='ui two buttons'>
                                    <Button  color='teal'>Contact</Button>
                                </div>
                            </Card.Content>
                        </Card>
                        <Card>
                            <Card
                                image={img2}
                                header='2017 Prado Jeep'
                                meta='On 4/16/18 10:15am'
                                description='Found at Los Angeles, CA, USA'/>
                            <Card.Content extra>
                                <div className='ui two buttons'>
                                    <Button  color='teal'>Contact</Button>
                                </div>
                            </Card.Content>
                        </Card>
                        <Card>
                            <Card
                                image={img2}
                                header='2017 Prado Jeep'
                                meta='On 4/16/18 10:15am'
                                description='Found at Los Angeles, CA, USA'/>
                            <Card.Content extra>
                                <div className='ui two buttons'>
                                    <Button  color='teal'>Contact</Button>
                                </div>
                            </Card.Content>
                        </Card>
                        <Card>
                            <Card
                                image={img2}
                                header='2017 Prado Jeep'
                                meta='Friend'
                                description='Found at Los Angeles, CA, USA'/>
                            <Card.Content extra>
                                <div className='ui two buttons'>
                                    <Button  color='teal'>Contact</Button>
                                </div>
                            </Card.Content>
                        </Card>
                        <Card>
                            <Card
                                image={img2}
                                header='2017 Prado Jeep'
                                meta='On 4/16/18 10:15am'
                                description='Found at Los Angeles, CA, USA'

                            />
                            <Card.Content extra>
                                <div className='ui two buttons'>
                                    <Button  color='teal'>Contact</Button>
                                </div>
                            </Card.Content>
                        </Card>
                    </Card.Group>




                    {/*<Image.Group size='medium'>*/}
                        {/*<Image src={img1} />*/}
                        {/*<Image src={img2} />*/}
                        {/*<Image src={img3} />*/}
                        {/*<Image src={img4} />*/}
                    {/*</Image.Group>*/}
                    {/*<Divider hidden />*/}
                    {/*<Image.Group size='medium'>*/}
                        {/*<Image src={img1} />*/}
                        {/*<Image src={img2} />*/}
                        {/*<Image src={img3} />*/}
                        {/*<Image src={img4} />*/}
                    {/*</Image.Group>*/}


                </Container>



            </div>
        )
    }
}