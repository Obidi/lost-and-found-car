import React, { Component } from 'react'
import {Form, Container, Segment, Divider, Button, Transition, Select,} from 'semantic-ui-react'



export default class FoundCarPage extends  Component{
    state = { visible: false };

    toggleVisibility = () => this.setState({ visible: !this.state.visible });

    handleOpen = () => {
        this.setState({ visible: true });

        this.timeout = setTimeout(() => {
            this.setState({ visible: false })
        }, 2500);
    };

    handleClose = () => {
        this.setState({ visible: false })
        clearTimeout(this.timeout)
    };


    render(){

        const { visible } = this.state;

        return(
        <div className='lost'>
                <Container>
                    <h1 style={{paddingTop: '5em'}}>Reporting a Found  Car?</h1>
                    <Divider />
                    <Divider hidden/>

                    <Form style={{marginTop: '5em'}}>
                        <Form.Group >
                            <Form.Select color='blue' id='form-input-control-first-name' control={Select} label='What type of Car did you find?' placeholder='Car Name' width={8}/>
                            <Form.Input type='date' label='Found On:' placeholder='Date' width={4}/>
                            <Form.Input type='time' label='At Exactly:' placeholder='Time' width={4}/>
                        </Form.Group>

                        <Form.TextArea id='form-textarea-control-opinion'  label='Where did you find it?' placeholder='Street, town and state' />
                        <Form.Input type='file' label='Upload an Image of the Car you found' />
                        <Form.Group>
                        <Form.Input  width={14} label='Email' placeholder='Your e-mail please, (we wont make it public to anyone)' />
                        <Form.Field onClick={this.toggleVisibility}  width={2} control={Button} color='teal' size='large' content='Report' label='Click to Report' />
                        </Form.Group>
                    </Form>



                    <Transition
                        unmountOnHide={true}
                        onShow={this.handleOpen}
                        onHide={this.handleClose}
                        visible={visible}
                        animation='drop' duration={500}>
                        <Segment  style={{ left: '80%', padding: '1.5em',
                            color: 'white',backgroundColor: 'red',
                            position: 'fixed', top: '8%', zIndex: 1000 }}>
                            <h3>Thanks for Reporting</h3>
                        </Segment>
                    </Transition>

                </Container>



            </div>
        )
    }
}