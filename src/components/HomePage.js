import React, { Component } from 'react'
import {Route, Link} from  'react-router-dom'
import { Button,
         Segment,
         Container,
         Grid,
        Divider

} from 'semantic-ui-react'



export default class HomePage extends  Component{

    render(){
        return(
            <div className='home'>

                <Segment basic>
                    <Container>
                        <h1 style={{paddingTop: '2em', fontSize: '5em'}}>
                            Lost And Found Cars</h1>
                        <Divider />
                        <Grid celled style={{marginTop: '5em'}}>

                            <Grid.Column width={8}>
                                <h2>Lost a Car? <span>Search here</span></h2>
                                <Button as={Link} to="lost" size='massive' fluid icon='car'  color='red'>
                                   I LOST A CAR!
                                </Button>
                            </Grid.Column>

                            <Grid.Column width={8}>
                                <h2 >Found a Car?  <span>Share here</span></h2>
                                <Button as={Link} to="found" size='massive' fluid icon='car'  color='teal'>
                                    FOUND A CAR!
                                </Button>
                            </Grid.Column>

                        </Grid>

                    </Container>

                </Segment>

            </div>
        )
    }
}