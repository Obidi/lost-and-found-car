import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './App.css'
import 'semantic-ui-css/semantic.min.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import registerServiceWorker from './registerServiceWorker';
import HomePage from "./components/HomePage";
import LostCarPage from "./components/LostCarPage";
import FoundCarPage from "./components/FoundCarPage";

const Root = () => {
    return   (
        <BrowserRouter>
            <div>
                <Switch>
                    <Route exact path='/' component={HomePage} />
                    <Route  path='/lost' component={LostCarPage} />
                    <Route  path='/found' component={FoundCarPage} />
                </Switch>

            </div>
        </BrowserRouter>
    )
};

ReactDOM.render(<Root />, document.getElementById('root'));
registerServiceWorker();

